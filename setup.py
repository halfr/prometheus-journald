import os
from setuptools import setup

setup(
    name="prometheus-journald",
    version="0.0.1",
    author="Rémi Audebert",
    author_email="rflah0@gmail.com",
    description="Exports journald metrics for the Prometheus monitoring system.",
    license="Apache",
    keywords="journald monitoring prometheus",
    url="https://bitbucket.org/halfr/prometheus-journald",
    packages=("prometheus_journald",),
    entry_points={'console_scripts': [ 'prometheus-journald = prometheus_journald.main:main' ]},
    install_requires=[
        "prometheus_client",
        "pip-prometheus>=1.0.0",
    ],
    dependency_links = ['https://github.com/prometheus/client_python/zipball/master#egg=prometheus_client'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: Information Technology",
        "Intended Audience :: System Administrators",
        "Topic :: System :: Monitoring",
        "License :: OSI Approved :: Apache Software License",
    ],
)

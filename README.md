# Prometheus journald exporter

Prometheus exporter for journald.

## Usage

### Requirements

* `python-systemd`
* `prometheus_client`

### Installation

Install with:

```shell
pip install prometheus-journald
```

### Quickstart

Run:

```shell
prometheus-journald
```

Visit: http://localhost:9010

By default, prometheus-journald exports:

* The size of the journal

### Adding custom rules

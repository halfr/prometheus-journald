#!/usr/bin/env python3
from http.server import HTTPServer
import argparse
import os
import random
import select
import threading
import time

from prometheus_client import Gauge, Counter, MetricsHandler

# Export metrics about installed packages
import pip_prometheus

from systemd import journal

log = journal.stream('prometheus-journald')

class LogEntryCount(threading.Thread):

    def __init__(self, metric):
        super().__init__()

        self.metric = metric

    def run(self):
        while True:
            event = self.reader.get_next()

            if not event:
                self.reader.wait()
                continue

            try:
                labels = {'unit': event['_SYSTEMD_UNIT'], 'priority': event['PRIORITY']}
            except KeyError:
                labels = {}

            self.metric.labels(labels).inc()


class LogEntryCountAll(LogEntryCount):
    def __init__(self, metric):
        super().__init__(metric)

        self.reader = journal.Reader()
        self.reader.seek_tail()
        # See https://bugs.freedesktop.org/show_bug.cgi?id=64614
        self.reader.get_previous()


class LogEntryCountMatch(LogEntryCount):
    def __init__(self, metric, unit):
        super().__init__(metric)

        self.unit = unit

        self.reader = journal.Reader()
        self.reader.add_match('_SYSTEMD_UNIT={}'.format(unit))
        self.reader.seek_tail()
        # See https://bugs.freedesktop.org/show_bug.cgi?id=64614
        self.reader.get_previous()


def parse_args():
    parser = argparse.ArgumentParser(description='prometheus-journald')

    default_host = os.environ.get('PROMETHEUS_JOURNALD_HOST', '0.0.0.0')
    parser.add_argument('--host', help='listen address', default=default_host)

    default_port = os.environ.get('PROMETHEUS_JOURNALD_PORT', '9010')
    parser.add_argument('-p', '--port', type=int, help='listen port',
                        default=int(default_port))

    default_units = os.environ.get('PROMETHEUS_JOURNALD_UNITS', '').split()
    parser.add_argument('-u', '--unit', metavar='U', action='append',
                        help='export log count for unit U',
                        default=default_units)
    default_all = os.environ.get('PROMETHEUS_JOURNALD_ALL', 'True').split()
    parser.add_argument('-a', '--all', action='store_true',
                        help='export log count for all units',
                        default=bool(default_all))

    args = parser.parse_args()
    return args


def main():
    args = parse_args()

    reader = journal.Reader()

    journal_disk_usage_bytes = Gauge('journal_disk_usage_bytes',
                                     'Total disk space currently used by journal files')
    journal_disk_usage_bytes.set_function(lambda: reader.get_usage())

    journal_entry_count = Counter('journal_entry_count',
                                  'Total line of log for the journal entry',
                                  ['unit', 'priority'])

    if args.unit:
        threads = []
        for unit in args.unit:
            log.write("Exporting: {}\n".format(unit))
            t = LogEntryCountMatch(journal_entry_count, unit)
            t.daemon = True
            t.start()
    elif args.all:
        log.write("Exporting all units\n")
        t = LogEntryCountAll(journal_entry_count)
        t.daemon = True
        t.start()

    httpd = HTTPServer((args.host, args.port), MetricsHandler)
    httpd.serve_forever()


if __name__ == '__main__':
    main()
